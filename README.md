<p align="center">
  <img width="290" height="auto" src="https://user-images.githubusercontent.com/108969749/201536991-5e528411-e91f-48a6-83cc-456a1d5dba18.png">
</p>

### Spesifikasi Hardware :
NODE  | CPU     | RAM      | SSD     |
| ------------- | ------------- | ------------- | -------- |
| Mainnet | 4          | 8         | 256  |
### Open Port

```
ufw allow ssh && ufw allow 8456 && ufw enable
```
### Install Depencies
```
sudo apt update && sudo apt upgrade -y
sudo apt install -y build-essential libssl-dev libffi-dev git curl screen
```
### Install Python V3+
```
sudo apt install python3.9
```
### Install Pip & Python3 venv
```
curl -sSL https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py
sudo apt install python3.9-venv
```
```
sudo mkdir newrl-venv
cd newrl-venv
sudo python3.9 -m venv newrl-venv
```
```
source newrl-venv/bin/activate
```
### Build Binaries
```
git clone https://github.com/newrlfoundation/newrl.git
cd newrl
scripts/install.sh mainnet
```
### Start Node
```
screen -S newrl
scripts/start.sh mainnet
```
### Cek Wallet Address Node
```
python3 scripts/show_wallet.py
```
